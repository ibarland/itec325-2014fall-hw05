﻿Hello, this really truly file is for RU ITEC325 2014fall hw05.
Note that it's **public** on bitbucket!

Give three statements about yourself (two true, one false).
*After* the hw05 due-date, you can edit this file to reveal the true fact.

ibarland #A: I was in the 1989 earthquake that took out a plank of the Golden Gate Bridge.
             http://en.wikipedia.org/wiki/1989_Loma_Prieta_earthquake
              those CA quakes are like, that you hear people mention over coffee.')
ibarland #B: I obtained every last star on Super Mario 3-D World.
ibarland #C: My Erdös number is 3, which I thought was pretty good, 
             until I found out my next-door neighbor's Erdös number is 2.

mthomas16 #A: I will complete the 4-year B.S. program in only 3 years.(T)
mthomas16 #B: English is not my first language. (F)
mthomas16 #C: I work for RU as a web developer. (T)

ejusticejr #A: I am 24 years old.
ejusticejr #B: I am a senior.
ejusticejr #C: I have brown eyes.

dnix1 #A: I've never fown on an airplane.
dnix1 #B: I was born in Australia.
dnix1 #C: I work at dish network.

dtaylor1 #A: I am from Maryland. (false)
dtaylor1 #B: I have a Legend of Zelda 3DS XL, I’ll post a picture. (true)
dtaylor1 #C: I am an intern at RCPS IT Department. (true)

aswain1 #A: When I was in the 10th grade, my appendix ruptured and I spent
            11 days in the hospital. I almost died. (True)
aswain1 #B: I have beaten the incredibly difficult "Dark Souls" video game. (False)
aswain1 #C: I was part of the cast of the first high school in America to
            perform the musical "White Christmas" when it came off of Broadway. (True)

wweston #A: I have visited Europe before.
wweston #B: I have completed Pikmin 3 in 30 (in-game) days.
wweston #C: I work for RU as a LARC Tutor.

mdramos #A: I have been in the Army reserve for 3 years and have 3 to go
mdramos #B: I believe we are in the Matrix
mdramos #C: I briefly did MMA and am undefeated (1-0 ha) 

mmmabardy #A: I will be graduating this fall
mmmabardy #B: I am on a heroic raid team in WoW
mmmabardy #C: I hate bacon

agiles2 #A: My lip has been numb for four years
            (true)
agiles2 #B: i have the ability to dunk a basketball.
            (false)
agiles2 #C: i got a gold medal in a Brazillian Jiu-Jitsu tournament
            (true)

kwoods4 #A: I was on the dance team in High School (false)
kwoods4 #B: I have my license to carry a concealed weapon (true)
kwoods4 #C: I have been competitively swimming since I was 4 (true)

cguske #A: My favorite food is Italian food
cguske #B: I work in an Italian restaurant
cguske #C: My Zodiac sign is Virgo

ljones9 #A: Both of my parents were born on Christmas Day.
ljones9 #B: I accidentally bought a car off eBay when I was in middle school.
ljones9 #C: I was born in Royal Berkshire Hospital in Reading, England.

awhearley #A: I like cheerleading
awhearley #B: I like dogs
awhearley #C: I like mushrooms

imccraw #A: I have an above average artistic ability.
imccraw #B: I have been working on hw05.
imccraw #C: I missed last class.

nnparsons #A: I coach softball. 
nnparsons #B: I hate football.
nnparsons #C: I love the lake.

jhollingswort #A: I am a black belt in Tae Kwon do
jhollingswort #B: I am awful at programming
jhollingswort #C: I am extremely tall

ppatel1 #A: I have been to every single continent except for Antarctica and South America.
ppatel1 #B: I played football in high school.
ppatel1 #C: I own a BMW

rcanevari2 #A: I once broke my foot by falling off the side of a moving car (True)
rcanevari2 #B: I was once one an olympic level shooting team (True)
rcanevari2 #C: I am fluent in spanish (False)

wstjohn #A: My car is almost as old as I am
wstjohn #B: Never traveled outside the contiguous United States
wstjohn #C: My right hand can touch my right elbow

mtilden #A: I went to see STS9 this weekend! 
mtilden #B: I love watching hockey and the Bruins.
mtilden #C: I really dont like to drive places.

mkiyf #A: I have visited Europe before
mkiyf #B: English is my first language
mkiyf #C: I played some football in high school

zdouglass #A: My family owns the largest maple syrup farm in Vermont. (F)
zdouglass #B: I once owned a beta fish named Swampy. (T)
zdouglass #C: I love riding motorcycles. (T)

kshah #A. I am attending graduate school.
kshah #B. I live in Blacksburg, va
kshah #c. I love watching movies at movie theater!!
