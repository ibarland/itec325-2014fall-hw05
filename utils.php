<?php
/** A set of utility functions, for php-generating-html.
 * @author Ian Barland
 * @version 2013.Feb.16
 */

/** Create (the html text for) a link tag.  Use the the URL as the tag-text,.
 * @param $url (string) The URL to link to.
 * @param $linkTxt  (string-or-false) The link-text, or false as a sentinel
 *    indicating that url should also be used as the link-text.
 * @return (the html text for) a link tag.
 */
function hyperlink($url,$linkTxt=false) {
  $theRealLinkTxt = ($linkTxt === false ? $url : $linkTxt);
  return "<a href='$url'>$theRealLinkTxt</a>";
  }

/** Correctly pluralize a noun (or not).
 * @param $num  How many of the $noun we have.
 * @param $noun  The noun to pluralize.
 * @return A string with the number&noun, correctly pluralized.
 */
function pluralize($num, $noun) {
  $theSuffix = (abs($num) == 1 ? "" : "s");
  return "$num $noun$theSuffix";
  }

/** Thumbnail: return HTML for an image with small width, which links to the full-size image.
 * @param $imgURL The URL for the image file.
 * @param $width The desired width for the thumbnail.
 * @return HTML for an image with small width, which links to the full-size image.
 */
function thumbnail( $imgURL, $width ) {
  return hyperlink( $imgURL, "<img src='$imgURL' width='${width}px' />" );
  }


/* commaList : string[] -> string
 * Given a bunch of items, return a comma-separated
 * list of all the individual strings, suitable for English.
 */
function commaList( $items ) {
  $strSoFar = "";
  $i = 0;
  foreach( $items as $item ) {
    $strSoFar .= (($i==0 || count($items)==2) ? "" : ",") 
               . (($i==0) ? "" : " ")
               . (($i < count($items)-1 || count($items) == 1) ? "" : "and ") 
               . $item;
    ++$i;
    }
  return $strSoFar; 
  }



/* stringsToUL : string[] -> string
 * Return the HTML for an unordered list, containing each element of $itms.
 */
function stringsToUL( $itms ) {
  $lineItemsSoFar = "";
  foreach ($itms AS $itm) {
    $lineItemsSoFar .= "  <li>$itm</li>\n";
    }
  return "<ul>\n" . $lineItemsSoFar . "</ul>\n";
  }


/* toc : string[] -> string
 * Return the HTML for a list of links-to-anchors,
 * given id's of elements in the current document.
 */
function toc( $itms ) {
  $linkItms = array();
  foreach( $itms AS $key => $val ) {
    $linkItms[$key] = hyperlink('#'.$val, $val);
    }
  return stringsToUL( $linkItms );
  }


?>
