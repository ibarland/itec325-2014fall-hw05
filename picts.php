<?php require_once('utils.php'); ?>

<html>
  <head>
    <title>hw05 pictures</title>
  </head>

  <body>

  <p>
    Here are pictures submitted by various students for hw05 / git practice.
   (You can generate this file via <tt>php picts.php >! picts.html</tt>.)
    <ul>
      <li>
	Food for thought?:<br/>
         <?php echo thumbnail('planter.jpg', 100); ?>
      </li>
      <li>
	This is a picture:<br/>
         <?php echo thumbnail('Penguins.jpg', 100); ?>
      </li>
	  <li>
        Target used in Project 5 for ITEC 220, a long, long time ago:<br/>
         <?php echo thumbnail('target.jpg', 100); ?>
      </li>
      <li>
        A picture of my Nintendo 3DS XL:<br/>
         <?php echo thumbnail('my3ds.jpg', 100); ?>
      </li>
	  <li>
		A picture:<br />
			<?php echo thumbnail('awesome.jpg', 100); ?>
	  </li>
      <li>
	What happens when I get bored and play with the iPhone camera filters:<br/>
         <?php echo thumbnail('swain.jpg', 100); ?>
      </li>
	  <li>
		My feelings about CSS: <br />
		 <?php echo thumbnail('mmmabardy.gif', 100); ?>
	  </li>
	  <li>
	  	My Legend of Zelda 3DS XL:<br/>
	  	 <?php echo thumbnail('dtaylor1.jpg', 100); ?>
	  </li>
	  <li>
		Life with a girlfriend:  <br />
		 <?php echo thumbnail('agiles2.jpg', 100); ?>
	  </li>
	  <li>
	Picture of a horse which I got to feed in the stables:<br/>
         <?php echo thumbnail('horse.jpg', 100); ?>
      </li>
      <li>
		Guess we're taking the boat to school:<br/>
		 <?php echo thumbnail('NewRiver.jpg', 100); ?>
	  </li>
	  <li>
       <li>
        A picture of a wave:<br/>
         <?php echo thumbnail('wave.png', 100); ?>
      </li>
		The officers of RU's Cyber Defense Club competing in the UCONN CyberSEED CTF:<br/>
         <?php echo thumbnail('ljones9.jpg', 100); ?>
      </li>
      <li>
		Cute Puppies!!:<br/>
         <?php echo thumbnail('Dog-Puppys.jpg', 100); ?>
      </li>
      <li>
		Dinosaur.<br/>
	<?php echo thumbnail('dinosaur.jpg', 100); ?>
      </li>
      <li>
                The best food ever from taco bell!!!:<br/>
        <?php echo thumbnail('quesadilla.png', 100); ?>
      </li>
		<li>
			Linux!!!!!<br/>
			<?php echo thumbnail('rcanevari2.jpeg', 100); ?>
		<li/>
      <li>
		Niagara Falls Panoramic Shot From Canada!<br/>
		<?php echo thumbnail('NiagaraFalls.jpg', 100); ?>
      </li>
      <li>
		Not an ad or anything:<br/>
		<?php echo thumbnail('coca-cola-125L.jpg', 100); ?>
      </li>
      <li>
               Kitty Tax:<br/>
                <?php echo thumbnail('kitty.jpg', 100); ?>
      </li>	
		<li>
		I made this!<br />
		<?php echo thumbnail('imadethis.jpg', 100); ?>
		</li>

      <li>
         I knew Mighty Mouse would finally show up! <br/>
         <?php echo thumbnail('mighty.jpg', 100); ?>
      </li>
	  
	   <li>
	     dogs-dog-pitbulldog!!
		 <?php echo thumbnail('dog.jpg', 100); ?>
	   </li>
</ul>
  </p>

  <p>
    Do NOT add any php code which attacks <tt>php.radford.edu</tt> via a denial-of-service-attack,
    inadvertent or not.
    (Um, don't attack any other sites, either.)
  </p>


  </body>
</html>
